/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hrizkiou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 19:43:52 by hrizkiou          #+#    #+#             */
/*   Updated: 2019/12/06 01:04:16 by hrizkiou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include "ft_ls.h"

int main(void) 
{
	DIR *dir;
	t_dir *entry;
	dir = opendir(".");
	entry = readdir(dir);
	while ((entry = readdir(dir)) != NULL) 
	{
		if (entry->d_type == DT_REG)
		{
			if (ft_strcmp(entry->d_name, ".") == 0 || ft_strcmp(entry->d_name, "..") == 0)
				                continue;
			ft_putendl(entry->d_name);
		}
	}
	return 0;
}
