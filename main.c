/* ************************************************************************************************ */
/*                                                                                                  */
/*                                                        :::   ::::::::   ::::::::  :::::::::::    */
/*   main.c                                            :+:+:  :+:    :+: :+:    :+: :+:     :+:     */
/*                                                      +:+         +:+        +:+        +:+       */
/*   By: hamza <hamza@student.1337.ma>                 +#+      +#++:      +#++:        +#+         */
/*                                                    +#+         +#+        +#+      +#+           */
/*   Created: 2019/11/24 20:00:18 by hamza           #+#  #+#    #+# #+#    #+#     #+#             */
/*   Updated: 2019/11/24 20:00:18 by hamza        ####### ########   ########      ###.ma           */
/*                                                                                                  */
/* ************************************************************************************************ */

#include "./includes/ft_ls.h"

int				main(int ac, char **av)
{
	int			i;
	int			flags;
	int			no_files;

	if ((i = parsing(ac, av, &flags)) == -1)
		return (1);
	av += i;
	ac -= i;
        printf("%d",i);   
}