/* ************************************************************************************************ */
/*                                                                                                  */
/*                                                        :::   ::::::::   ::::::::  :::::::::::    */
/*   parsing.c                                         :+:+:  :+:    :+: :+:    :+: :+:     :+:     */
/*                                                      +:+         +:+        +:+        +:+       */
/*   By: hamza <hamza@student.1337.ma>                 +#+      +#++:      +#++:        +#+         */
/*                                                    +#+         +#+        +#+      +#+           */
/*   Created: 2019/11/24 20:00:18 by hamza           #+#  #+#    #+# #+#    #+#     #+#             */
/*   Updated: 2019/11/24 20:00:18 by hamza        ####### ########   ########      ###.ma           */
/*                                                                                                  */
/* ************************************************************************************************ */

#include "./includes/ft_ls.h"

int			parsing(int ac, char **av, int *flags)
{
	int	i;

	*flags = 0;
	i = 0;
	while (++i < ac && av[i][0] == '-' && av[i][1])
	{
		if (av[i][1] == '-' && av[i][2])
			return (i + 1);
		//if (!parse_options(av[i], flags))
			//return (-1);
	}
	return (i);
}
// int			parse_options(char *s, int *flags)
// {
// 	int		n;

// 	while (*(++s))
// 	{
// 		if ((n = ft_strchr_index("alRrtdG1Ss", *s)) == -1)
// 			ls_error(s, USAGE);
// 		*flags |= (1 << n);
// 		if ((*s == 'l') || (*s == '1'))
// 			*flags &= (*s == 'l') ? ~LS_ONE : ~LS_L;
// 	}
// 	return (1);
// }