/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_info.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hrizkiou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 19:43:52 by hrizkiou          #+#    #+#             */
/*   Updated: 2019/12/06 21:24:35 by hrizkiou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int main (int ac, char *av[])
{
	DIR *dir;
	t_dir *dirent;
	t_info buff;
	t_pw *passwd;
	t_gr *group;
	int s;
	//dirent = readdir(dir);
	if (ac == 1)
		s = stat(".", &buff);
	else
		s = stat(av[1],&buff);
	passwd = getpwuid(buff.st_uid);
	group = getgrgid(buff.st_gid);
	ft_putstr("name : ");
	ft_putendl(av[1]);
	ft_putstr("Taille : ");
	ft_putnbr(buff.st_size);
	ft_putendl(" bytes");
	ft_putstr("Modes :");
	//ft_putendl();
	ft_putstr("\nowner : ");
	ft_putendl(passwd->pw_name);
	ft_putstr("group : ");
	ft_putendl(group->gr_name);
	//ft_putstr("Date de derniere modification : ");
	printf("Modification time  = %s", ctime(&buff.st_mtime));
	//printf("%d",dirent->d_type);
	return (0);
}
