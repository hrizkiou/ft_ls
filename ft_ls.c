/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hrizkiou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 19:43:52 by hrizkiou          #+#    #+#             */
/*   Updated: 2019/12/07 01:18:26 by hrizkiou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	listcontent(t_dir *f)
{
	DIR *dir;
	char *name;
	//t_info *info;

	dir = opendir(".");
	while ((f = readdir(dir)) != NULL)
	{
		name = f->d_name;
		if (!ft_strcmp(name,".") || !ft_strcmp(name,".."))
			continue;
		if (f->d_type == DT_DIR)
		{
			//printf(BLEU "%s\t" RESET ,f->d_name);
			ft_putstr(BLEU);
			ft_putstr(f->d_name);
			ft_putstr(RESET);
			ft_putchar('\t');
		}
		else 
		{
			ft_putstr(f->d_name);
			ft_putchar('\t');
		}
	}
	ft_putchar('\n');
}
int main (int ac, char *av[])
{
	t_dir *f;
	t_opt *opt;

	if (ac > 1)
		printf("ok");
	else
		listcontent(f);
	return (0);
}
