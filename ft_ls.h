/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hrizkiou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 19:41:59 by hrizkiou          #+#    #+#             */
/*   Updated: 2019/12/07 01:05:25 by hrizkiou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft/libft.h" 
# include <sys/stat.h>
# include <dirent.h>
# include <unistd.h>
# include <string.h>
# include <stdlib.h>
# include <stdio.h>
# include <sys/types.h>
# include <pwd.h>
# include <uuid/uuid.h>
# include <grp.h>
# include <errno.h>
# include <time.h>

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define BLEU 	"\x1b[1;36m"
#define RESET   "\x1b[0m"

typedef struct dirent t_dir;
typedef struct stat	t_info;
typedef struct passwd t_pw;
typedef struct group t_gr;
typedef struct s_opt
{
	int l;
	int R;
	int a;
	int r;
	int t;
}				t_opt;
#endif
