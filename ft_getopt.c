/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hrizkiou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 19:43:52 by hrizkiou          #+#    #+#             */
/*   Updated: 2019/12/07 00:24:54 by hrizkiou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void listdir(char *path, size_t size) 
{
	DIR *dir;
	t_dir *entry;
	size_t len = ft_strlen(path);

	if (!(dir = opendir(path))) 
	{
		fprintf(stderr, "path not found: %s: %s\n",
				path, strerror(errno));
		return;
	}
	ft_putstr(path);
	while ((entry = readdir(dir)) != NULL) 
	{
		char *name = entry->d_name;
		if (entry->d_type == DT_DIR) 
		{
			if (!strcmp(name, ".") || !strcmp(name, ".."))
				continue;
			if (len + strlen(name) + 2 > size) 
				fprintf(stderr, "path too long: %s/%s\n", path, name);
			else 
			{
				path[len] = '/';
				ft_strcpy(path + len + 1, name);
				listdir(path, size);
				path[len] = '\0';
			}
		} 
		else 
			printf("%s\t", name);
	}
	closedir(dir);
}

int main(void) 
{
	char path[1024] = ".";
	listdir(path, sizeof path);
	printf("\n");
	return (0);
}
