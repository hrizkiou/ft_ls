#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#define MAXC 64

void dump_tm (struct tm *t);

int main(int argc, char **argv)
{
	int i = 0;
	char time_str[MAXC] = "";
	time_t now = time (NULL);
	struct stat sb;
	struct tm tmfile, tmnow;

	if (argc < 2) {     /* validate sufficient arguments provided */
		fprintf (stderr, "error: insufficient input, usage: %s <pathname>\n",
				argv[0]);
		return 1;
	}

	for (i = 1; i < argc; i++) {    /* for each file given as arg */
		if (stat(argv[i], &sb) == -1) {  /* validate stat of file */
			perror("stat");
			return 1;
		}

		localtime_r (&sb.st_mtime, &tmfile);    /* get struct tm for file */
		localtime_r (&now, &tmnow);             /* and now */

		if (tmfile.tm_year == tmnow.tm_year) {    /* compare year values  */
			strftime (time_str, sizeof (time_str), "%b %e %H:%M",
					&tmfile);   /* if year is current output date/time  */
			printf ("permission 1 user group 12345 %s %s\n",
					time_str, argv[i]);
		}
		else { /* if year is not current, output time/year */
			strftime (time_str, sizeof (time_str), "%b %e  %Y",
					&tmfile);
			printf ("permission 1 user group 12345 %s %s\n",
					time_str, argv[i]);
		}
	}

	return 0;
}
