/* ************************************************************************************************ */
/*                                                                                                  */
/*                                                        :::   ::::::::   ::::::::  :::::::::::    */
/*   ft_ls.h                                           :+:+:  :+:    :+: :+:    :+: :+:     :+:     */
/*                                                      +:+         +:+        +:+        +:+       */
/*   By: hamza <hamza@student.1337.ma>                 +#+      +#++:      +#++:        +#+         */
/*                                                    +#+         +#+        +#+      +#+           */
/*   Created: 2019/11/24 20:01:44 by hamza           #+#  #+#    #+# #+#    #+#     #+#             */
/*   Updated: 2019/11/24 20:01:44 by hamza        ####### ########   ########      ###.ma           */
/*                                                                                                  */
/* ************************************************************************************************ */

#ifndef FT_LS_H
#define FT_LS_H

# include <sys/types.h>
# include <dirent.h>
# include <string.h>
# include <stdio.h>
# include <unistd.h>

typedef struct dirent	t_dir;

int			parsing(int ac, char **av, int *flags);

#endif